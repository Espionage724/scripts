#!/bin/bash

sed -i 's/XDG_DOCUMENTS_DIR=".*"/XDG_DOCUMENTS_DIR="$HOME\/Personal\/Documents"/'
sed -i 's/XDG_PICTURES_DIR=".*"/XDG_PICTURES_DIR="$HOME\/Personal\/Pictures"/'
sed -i 's/XDG_VIDEOS_DIR=".*"/XDG_VIDEOS_DIR="$HOME\/Personal\/Videos"/'
