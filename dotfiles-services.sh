#!/bin/bash

if [[ -f /usr/lib/systemd/user/proton-bridge.service ]]; then
	systemctl enable --user --now proton-bridge.service
fi

systemctl enable --user --now email-oauth2-proxy@$USER.service

systemctl enable --user --now syncthing.service
