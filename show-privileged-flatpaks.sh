#!/bin/bash

declare -a permissions=("home" "host")
	
for permission in ${permissions[@]}; do
	for app in $(flatpak list --columns=ref); do
		if [[ $(flatpak info --show-permissions $app | grep filesystems) == *"$permission"* ]]; then
			echo "https://flathub.org/apps/$(echo $app | sed 's|/.*||') has the $permission permission"
		fi
	done
done
